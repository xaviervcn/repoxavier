// @ts-check

const { test, expect } = require('@playwright/test');

test('Check website title, link and alert', async ({ page }) => {
  // Go to the website
  await page.goto('https://www.academyoflearning.com/');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Academy/)

  // Click on a link and verify that it works
  await Promise.all([
    page.waitForNavigation(),
    page.click('a'),
  ]);
  expect(page.url()).toBe('https://www.academyoflearning.com/');

  // Check that an alert is generated with the expected text
  page.on('dialog', async dialog => {
    expect(dialog.message()).toBe('First Name Required.');
    await dialog.accept();
  });
  await page.evaluate(() => alert('First Name Required.'));
});
